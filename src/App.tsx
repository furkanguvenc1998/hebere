/*
 * Copyright 2018-2021 Commencis. All Rights Reserved.
 *
 * Save to the extent permitted by law, you may not use, copy, modify,
 * distribute or create derivative works of this material or any part
 * of it without the prior written consent of Commencis.
 * Any reproduction of this material must contain this notice.
 */

import React from 'react';

import Routes from 'routes/Routes';
import IdendityProvider from 'common/IdendityProvider';
import ThemeProvider from 'common/theme/ThemeProvider';
import 'common/i18n/init';

const App = (): JSX.Element => (
  <IdendityProvider>
    <ThemeProvider>
      <Routes />
    </ThemeProvider>
  </IdendityProvider>
);

export default App;
