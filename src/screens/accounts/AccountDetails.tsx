import React from 'react';

import GenericView from 'components/GenericView';

const AccountDetails = () => {
  return <GenericView text="Account Details" />;
};

export default AccountDetails;
