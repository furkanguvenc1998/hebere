import React from 'react';
import {Button} from 'react-native';
import PATHS from 'common/paths';
import {AccountStackProps} from 'routes/Routes';
import {StackNavigationProp} from '@react-navigation/stack';
import {useTheme} from 'common/theme/ThemeProvider';
import GenericView from 'components/GenericView';

type Props = {
  navigation: StackNavigationProp<AccountStackProps, PATHS.ACCOUNTS>;
};

const Accounts = ({navigation}: Props) => {
  const {theme} = useTheme();
  return (
    <GenericView text="Accounts">
      <Button
        color={theme.button.buttonColor}
        title="Go to Account Details"
        onPress={() => navigation.navigate(PATHS.ACCOUNT_DETAILS)}
      />
    </GenericView>
  );
};

export default Accounts;
