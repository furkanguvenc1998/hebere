import React from 'react';

import GenericView from 'components/GenericView';
import {Button} from 'react-native';
import {useIdentity} from 'common/IdendityProvider';

const SmsOtp = () => {
  const {login} = useIdentity();
  return (
    <GenericView text="SMS OTP">
      <Button
        color="orange"
        title="Login"
        onPress={() => login({username: 'Test'})}
      />
    </GenericView>
  );
};

export default SmsOtp;
