import React from 'react';

import GenericView from 'components/GenericView';
import PATHS from 'common/paths';
import {Button} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {AccountStackProps} from 'routes/Routes';
import {useTheme} from 'common/theme/ThemeProvider';

type Props = {
  navigation: StackNavigationProp<AccountStackProps, PATHS.ACCOUNTS>;
};

const Login = ({navigation}: Props) => {
  const {theme} = useTheme();
  return (
    <GenericView text="Login">
      <Button
        color={theme.button.buttonColor}
        title="Go to Sms Otp"
        onPress={() => navigation.navigate(PATHS.SMS_OTP)}
      />
    </GenericView>
  );
};

export default Login;
