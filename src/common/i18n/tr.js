const localizations = {
  translation: {
    toggle_language: 'Dili Değiştir',
    login_navigationBar_title: 'GİRİŞ',
    login_username_textfield_placeholder: 'Kullanıcı Adı',
    login_password_textfield_placeholder: 'Şifre',
    login_rememberMe_label_text: 'Beni Hatırla',
    login_login_button_title: 'Giriş',
    login_login_alert_okButton_title: 'Tamam',
    login_login_alert_cancelButton_title: 'İptal',
    login_languageSelection_TR_title: 'TR',
    login_languageSelection_EN_title: 'EN',
  },
};

export default localizations;
