const localizations = {
  translation: {
    toggle_language: 'Toggle Language',
    login_navigationBar_title: 'LOGIN',
    login_username_textfield_placeholder: 'Username',
    login_password_textfield_placeholder: 'Password',
    login_rememberMe_label_text: 'Remember Me',
    login_login_button_title: 'Login',
    login_login_alert_okButton_title: 'OK',
    login_login_alert_cancelButton_title: 'Cancel',
    login_languageSelection_TR_title: 'TR',
    login_languageSelection_EN_title: 'EN',
  },
};

export default localizations;
