import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import EnglishLocalizations from 'common/i18n/en';
import TurkishLocalizations from 'common/i18n/tr';

export enum SUPPORTED_LANGUAGES {
  EN = 'en',
  TR = 'tr',
}

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: {
      [SUPPORTED_LANGUAGES.EN]: EnglishLocalizations,
      [SUPPORTED_LANGUAGES.TR]: TurkishLocalizations,
    },
    lng: SUPPORTED_LANGUAGES.EN,
    fallbackLng: SUPPORTED_LANGUAGES.TR,

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
