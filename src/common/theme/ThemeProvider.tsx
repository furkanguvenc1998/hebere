import React, {useContext, useState} from 'react';
import {Theme as RNTheme} from '@react-navigation/native';
import Colors from 'common/theme/Colors';
import {useColorScheme} from 'react-native';

type Theme = RNTheme & {
  button: {
    buttonColor: string;
    disabledColor: string;
  };
};

const LightTheme: Theme = {
  dark: true,
  colors: {
    primary: Colors.softBlue,
    background: 'white',
    card: Colors.paleGray,
    text: 'black',
    border: Colors.softBlue,
    notification: 'white',
  },
  button: {
    buttonColor: Colors.softBlue,
    disabledColor: Colors.softBlue,
  },
};

const DarkTheme: Theme = {
  dark: false,
  colors: {
    primary: Colors.darkPeriwinkle, // primaryColor
    background: Colors.dusk, // scaffoldBackgroundColor
    card: Colors.twilight, // cardColor
    text: 'white', // textTheme color
    border: Colors.darkPeriwinkle,
    notification: Colors.darkGrayBlueTwo, // appBarTheme color
  },
  button: {
    buttonColor: Colors.darkPeriwinkle,
    disabledColor: Colors.darkPeriwinkle,
  },
};

type ContextType = {
  theme: Theme;
  toggleTheme: () => void;
};

const ThemeContext = React.createContext<ContextType>({
  theme: LightTheme,
  toggleTheme: () => {},
});

type Props = {
  children: React.ReactElement;
};

const ThemeProvider = ({children}: Props) => {
  const scheme = useColorScheme(); // OS preferences

  const [currentTheme, setCurrentTheme] = useState(
    scheme === 'dark' ? DarkTheme : LightTheme,
  );

  const toggleTheme = () => {
    setCurrentTheme(prevTheme =>
      prevTheme === DarkTheme ? LightTheme : DarkTheme,
    );
  };

  return (
    <ThemeContext.Provider value={{theme: currentTheme, toggleTheme}}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeProvider;

export const useTheme = () => useContext(ThemeContext);
