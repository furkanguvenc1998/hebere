import React, {useContext, useState} from 'react';

type User = {
  username: string;
};

type ContextType = {
  user: User | null;
  login: (user: User) => void;
  logout: () => void;
};

const IdentityContext = React.createContext<ContextType>({
  user: null,
  login: () => {},
  logout: () => {},
});

type Props = {
  children: React.ReactElement;
};

const IdendityProvider = ({children}: Props) => {
  const [user, setUser] = useState<ContextType['user']>(null);

  const login = (user_: User) => {
    setUser(() => user_);
  };

  const logout = () => {
    setUser(_prevState => null);
  };

  return (
    <IdentityContext.Provider value={{user, login, logout}}>
      {children}
    </IdentityContext.Provider>
  );
};

export default IdendityProvider;

export const useIdentity = () => useContext(IdentityContext);
