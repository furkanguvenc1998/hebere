enum PATHS {
  LOGIN = 'Login',
  SMS_OTP = 'Sms Otp',
  ACCOUNTS = 'Accounts',
  ACCOUNT_DETAILS = 'Account Details',
}

export default PATHS;
