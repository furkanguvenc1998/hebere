import React from 'react';
import {createNativeStackNavigator} from 'react-native-screens/native-stack';
import PATHS from 'common/paths';
import Accounts from 'screens/accounts/Accounts';
import AccountDetails from 'screens/accounts/AccountDetails';
import {NavigationContainer} from '@react-navigation/native';
import {enableScreens} from 'react-native-screens';
import {useTheme} from 'common/theme/ThemeProvider';
import {useIdentity} from 'common/IdendityProvider';
import Login from 'screens/login/Login';
import SmsOtp from 'screens/login/SmsOtp';
// run this before any screen render(usually in App.js)
enableScreens();

export type AccountStackProps = Record<PATHS, undefined>;

const AccountStackNavigator = createNativeStackNavigator<AccountStackProps>();

const Routes = () => {
  const {theme} = useTheme();
  const {user} = useIdentity();

  return user !== null ? (
    <NavigationContainer theme={theme}>
      <AccountStackNavigator.Navigator>
        <AccountStackNavigator.Screen
          name={PATHS.ACCOUNTS}
          component={Accounts}
        />
        <AccountStackNavigator.Screen
          name={PATHS.ACCOUNT_DETAILS}
          component={AccountDetails}
        />
      </AccountStackNavigator.Navigator>
    </NavigationContainer>
  ) : (
    <NavigationContainer theme={theme}>
      <AccountStackNavigator.Navigator initialRouteName={PATHS.LOGIN}>
        <AccountStackNavigator.Screen name={PATHS.LOGIN} component={Login} />
        <AccountStackNavigator.Screen name={PATHS.SMS_OTP} component={SmsOtp} />
      </AccountStackNavigator.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
