import React from 'react';
import {Button, Text, View} from 'react-native';
import {useTheme} from 'common/theme/ThemeProvider';
import styles from 'common/theme/styles';
import {useTranslation} from 'react-i18next';
import {SUPPORTED_LANGUAGES} from 'common/i18n/init';

type Props = {
  text: string;
  children?: React.ReactElement;
};

const GenericView = ({children, text}: Props) => {
  const {theme, toggleTheme} = useTheme();
  const {t, i18n} = useTranslation();
  return (
    <View style={{...styles.View, backgroundColor: theme.colors.background}}>
      <Text style={{color: theme.colors.text}}>{text}</Text>
      <Button
        color="red"
        title={`Toggle Theme: ${theme.dark ? 'dark' : 'light'}`}
        onPress={() => toggleTheme()}
      />
      <Button
        color="blue"
        title={`${t('toggle_language')}: ${i18n.language}`}
        onPress={() =>
          i18n.changeLanguage(
            i18n.language === SUPPORTED_LANGUAGES.TR
              ? SUPPORTED_LANGUAGES.EN
              : SUPPORTED_LANGUAGES.TR,
          )
        }
      />
      {children}
    </View>
  );
};

export default GenericView;
